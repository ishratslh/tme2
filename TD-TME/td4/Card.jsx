//Exercice 1

import {useState} from "react";

props={ //properties
    symbol:"toto",
    name:"bob",
    test:true
}

function Card(props){
    const[affichage, setAffichage]=useState("hidden");

    const displayCard =() => { //vraie fleche attention
        if (affichage='hidden'){//egal 3
            setAffichage('visible')
        }else{
            setAffichage('hidden')
        }
    
    }
                                            //egal 3
    return <div onClick={displayCard}>{affichage = 'visible' ? props.symbol : '-'}</div>
} //(1.4)

export default Card