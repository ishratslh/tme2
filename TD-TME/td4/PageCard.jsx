import CardList from"./CardList"
import FormAddCarte from "./CardForm"

function PageCard(props){
    return(
        <div>
            <CardList/>
            <FormAddCarte/>
        </div>
    );
}

export default PageCard
