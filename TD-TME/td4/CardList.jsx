//Exercice 2
import Card from "./Card";
import {useState} from "react";

const CardList = (props) => {
    const[cartes, setCartes]=useState(
        [
            {id:1, card:"toto", feedback:'hidden'},
            {id:2, card:"tata", feedback:'hidden'},
            {id:3, card:"titi", feedback:'hidden'},
            {id:4, card:"tutu", feedback:'hidden'},
        ]
    );

    return <div id="CardList">
        {cartes.map( carte => (
            <Card id={carte.id} symbol={carte.card} affichage={carte.feedback}/>
    ))}
    
    <Card symbol={"toto"}/>
    <Card symbol={"titi"}/>
    <Card symbol={"tata"}/>
    <Card symbol={"tutu"}/>
</div>
}
