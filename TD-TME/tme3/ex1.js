//ouvrir ficher dans navigateur, "plus d'outils", "outils de dev"
//"defer" important dans le fichier html apres src

const p1 = document.getElementById("p1");
console.log(p1);

const class1 = document.getElementsByClassName("classe1");

const header = document.getElementsByTagName("header")[0]
console.log(header)

document.getElementsByTagName("h1").parentElement;
console.log(class1);

const p_class1 = document.querySelectorAll("p.class1")
console.log(p_class1)
const coll = document.querySelectorAll("main .classe1");
console.log(coll);

const enfant = document.getElementsByTagName("main")[0].children
console.log(enfant)

document.getElementsByTagName("main")[0].childNodes.length;

const nb_enfant = document.getElementsByTagName("main")[0].children.length
console.log(nb_enfant)

const sibling = document.getElementsByTagName("h2")[2].nextSibling
console.log(sibling)

document.getElementsByTagName("p")[document.getElementsByTagName("p").length-1];

const dernier = document.getElementsByTagName("main")[0].lastElementChild
console.log(dernier)

document.getElementsByTagName("h2")[2].nextSibling.nextSibling;



