import React from 'react';
import { useState } from 'react';

//import Livre from "./Livre1";
//import Livre from "./Livre2";

function App() {
	const dateAuj = new Date();
	const dateE = new Date(2023, 1, 19);
  	
  	const [titreCollection, setTitreCollection] = useState("");	
	const [livres, setLivres] = useState([
		{auteur: "Hugo Victor", titre: "La Légende des siècles", emprunt: {statut: false, dateEmprunt:dateAuj}, cote: "HUG001"},
		{auteur: "Hugo Victor", titre: "Les Misérables", emprunt: {statut: false, dateEmprunt:dateAuj}, cote: "HUG002"},
		{auteur: "Zola Émile", titre: "L'Assommoir", emprunt: {statut: true, dateEmprunt:dateE}, cote: "ZOL001"}
	]);

    const getTitle = (e) => { setTitreCollection(e.target.value) }
  	
    const handleTititreCollectionChange=(event) =>{
        console.log(event);
        setLivres("");
    }

    return (
      <div id="Principal">
        <h1>{titreCollection}</h1>
        <label htmlFor="pr">Titre de la collection? </label><input id="pr" onChange={getTitle} ></input> 
        </div> 	
    );


}