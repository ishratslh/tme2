import login from "./login";
import logout from "./logout";

function NavigationPanel(props){
    return(
    <div id="navigation_pan">
        {(props.isConnected) ? <logout logout={props.logout}/> : <login login={props.login}/>}
    </div>
    );
}

export default NavigationPanel;
