import "./App.css";
import {useState} from 'react';
import NavigationPanel from "./NavigationPanel";
import Login from "./login";
import Signin from "./signin";
import MessagePage from "./MessagePage";

function MainPage(props){
    //const[isConnected, setConnect]=useState([{isConnected:false}]);
    const[isConnected, setConnect]=useState(false);
    const[page, setPage]=useState([{page:"signin_page"}]);

    const getConnected=() =>{
        setConnect(true);
        setPage("message_page");
    }

    const setLogout=() =>{
        setConnect(false);
        setPage("signin_page");
    }

    return( 
        <div>
            {page === "login_page" ? <Login /> : <NavigationPanel login={getConnected} logout={setLogout} isConnected={isConnected} />}
            {page === "login_page" ? <Login getConnected={getConnected} /> : null}
            {page === "login_page" ? <Login setPage={setPage} /> : null}
            {page === "message_page" ? <MessagePage /> : null}
            {page==="signin_page"? <Signin /> : <NavigationPanel login={getConnected} logout={setLogout} isConnected={isConnected}/>}
        </div>
        

        
    );
}


export default MainPage;