import './App.css';
import {useState} from 'react';
import './login.css';

function Login(props){
    const[login, setLogin] = useState("");
    const[password, setPassword] = useState("");

    const getLogin = (evt) =>{setLogin(evt.target.value)}
    const getPassword = (evt) =>{setPassword(evt.target.value)}
    const handleLogin=() =>{}

    return(
        <div>
        <h1>Ouvrir une session</h1>
        <form method="POST" action="">
            <label htmlfor="Login">Login</label><input id="Login" onChange={getLogin} />
            
            <label htmlfor="Mot de passe">Mot de passe</label><input type="password" id="mdp" onChange={getPassword} />

            <button type="submit" onClick={handleLogin}>Connexion</button><button type="reset">Annuler</button>
        </form>
        
   
        </div>
    );
}

            /*<input type="submit" value="Connexion" />
            <input type="reset" value="Annuler" />*/

export default Login;