import {useState} from "react";
import './signin.css';

function Signin(props){
    const [login, setLogin]=useState("");
    const [password, setPassword]=useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [passOK, setPassOK] = useState(false);

    const [pass1, setPass1] = useState("");
    const [pass2, setPass2] = useState("");

    const getLogin = (evt) => {setLogin(evt.target.value)};
    const getFirstName = (evt) => {setFirstName(evt.target.value)};
    const getLastName = (evt) => {setLastName(evt.target.value)};
    const getPass1 = (evt) => {setPass1(evt.target.value)};
    const getPass2 = (evt) => {setPass2(evt.target.value)};

    const submissionHandler = (evt) => {
        if (pass1 === pass2) setPassOK(true);
    }

    return(
        <div>
        <h1>Enregistrement</h1>
        <form method="POST" action="javascript:connexion">
            <label htmlfor="firstname">Prénom</label><input id="firstname" onChange={getFirstName}/>
            <label htmlfor="lastname">Nom</label><input id="lastname" onChange={getLastName}/>
            
            <label htmlfor="signin_login">Login</label><input id="signin_login" onChange={getLogin} />
            
            <label htmlfor="signin_mdp1">Password</label><input type="password" id="signin_mdp1" onChange={getPass1} />
            <label htmlfor="signin_mdp2">Password2</label><input type="password" id="signin_mdp2" onChange={getPass2} />
            
            <label for="Retapez">Retapez</label><input type="retry" id="retry" />
            
            <button onClick={submissionHandler}>Sign In</button><button type="reset">Annuler</button>
            
            {passOK ? <p></p>:<p style={{color:"red"}}>Erreur: mots de passe différents</p>}
        </form>
        
        </div>
    );
}

export default Signin;