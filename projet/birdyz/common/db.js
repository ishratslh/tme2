const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017/mydb';

class Database {
    constructor() {
        this.db = null;
    }

    async connect() {
        if (this.db) return this.db;
        const client = await MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
        this.db = client.db();
        return this.db;
    }

    async disconnect() {
        if (this.db) {
            await this.db.client.close();
            this.db = null;
        }
    }
}

module.exports = new Database();